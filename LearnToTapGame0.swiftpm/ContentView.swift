import SwiftUI
import SpriteKit

class GameScene: SKScene {
    
    var Chillana = SKSpriteNode()
    var Ball = SKShapeNode()
    
    override func didMove(to view: SKView) {
        
        Ball = SKShapeNode()
        Ball.path = UIBezierPath(ovalIn: CGRect(x: -50, y: -50, width: 100, height: 100)).cgPath
        Ball.position = CGPoint(x: frame.midX, y: frame.midY)
        Ball.fillColor = UIColor.red
        Ball.strokeColor = UIColor.white
        Ball.lineWidth = 2
        addChild(Ball)
        
        
        Chillana = SKSpriteNode(texture: SKTexture(imageNamed: "ChillanaPic2"), color: .clear, size: CGSize(width:100 , height:100))
        
        Chillana.position = CGPoint(x: 500, y: frame.minY+50)
        addChild(Chillana)
        
        //print("Works")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        let location = touch.location(in: self)
        Chillana.position = location
        
        if Ball.contains(touch.location(in: self)){
            Ball.removeFromParent()
        }
        
        
    }
    
}



//The UI with all views is organized below
struct ContentView: View {
    
    var scene: SKScene {
        let scene = GameScene()
        scene.size = CGSize(width: 1024, height: 768)
        scene.backgroundColor = SKColor.systemYellow
        scene.scaleMode = .aspectFit
        return scene
    }
    
    var body: some View {
        VStack {
            
            HStack{
                Image(systemName: "globe")
                    .imageScale(.large)
                    .foregroundColor(.accentColor)
                Text("Sprite World!")
                Image(systemName: "globe")
                    .imageScale(.large)
                    .foregroundColor(.accentColor)
                
            }
            
            
            SpriteView(scene: scene)
            //.scaledToFit()
                .frame(width: 1024, height: 768)
                .ignoresSafeArea()
            
            Text("Press the screen and move your finger to move Chillana 😀")
        }
    }
}
